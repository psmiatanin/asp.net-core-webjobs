﻿using System;
using System.Threading.Tasks;

namespace DPG.Customer.Application.Contract
{
    public interface ICustomerService
    {
        Task DoSomeWorkAsync();
    }
}
