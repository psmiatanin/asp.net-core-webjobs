﻿using DPG.Customer.Application.Contract;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace DPG.Customer.Application.Implementation
{
    public class CustomerService : ICustomerService
    {
        private ILogger<CustomerService> _logger;

        public CustomerService(ILogger<CustomerService> logger)
        {
            _logger = logger;
        }

        public Task DoSomeWorkAsync()
        {

            return Task.CompletedTask;
        }
    }
}
