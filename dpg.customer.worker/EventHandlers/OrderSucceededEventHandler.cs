﻿using DPG.Customer.Application.Contract;
using DPG.Customer.Worker.Events;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace DPG.Customer.Worker.EventHandlers
{
    public class OrderSucceededEventHandler
    {
        private readonly ICustomerService _customerService;

        public OrderSucceededEventHandler(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        public async Task HandleAsync([ServiceBusTrigger("noti-ordersucceeded")]OrderSucceededEvent @event, 
            ILogger logger)
        {
            await _customerService.DoSomeWorkAsync();
            logger.LogDebug($"OrderSucceededEvent handled. OrderId={@event.OrderId}");
        }
    }
}
