﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DPG.Customer.Worker.Events
{
    public class OrderSucceededEvent
    {
        public Guid OrderId { get; set; }
    }
}
