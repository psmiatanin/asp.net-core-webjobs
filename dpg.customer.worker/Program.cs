﻿using DPG.Customer.Application.Contract;
using DPG.Customer.Application.Implementation;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System.IO;

namespace dpg.customer.worker
{
    class Program
    {
        static void Main(string[] args)
        {
            IConfiguration configuration = null;

            var hostBuilder = new HostBuilder()
                .ConfigureAppConfiguration(configurationBuilder => {
                    configurationBuilder
                        .SetBasePath(Directory.GetCurrentDirectory())
                        .AddEnvironmentVariables()
                        .AddJsonFile("appsettings.json");

                    // temporary
                    configuration = configurationBuilder.Build();
                })
                .ConfigureLogging(loggingBiolder => {
                    loggingBiolder.SetMinimumLevel(LogLevel.Debug);
                    loggingBiolder.AddConsole();
                })
                .ConfigureServices(services => {
                    services.AddTransient<ICustomerService, CustomerService>();
                })
                .ConfigureWebJobs(webJobsBuilder => {
                    webJobsBuilder.AddServiceBus(b => {
                        b.ConnectionString = configuration["AzureServiceBusConnectionString"];
                        b.MessageHandlerOptions.AutoComplete = true;
                    });
                });

            using (var host = hostBuilder.Build())
            {
                host.Run();
            }
        }

    }
}
